﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts
{
    public class Knife : MonoBehaviour, IDamager
    {
        public float damage = 1;

        public float Damage => damage;

        public MyHitEvent OnHit;

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }



        private void OnTriggerEnter2D(Collider2D collision)
        {
            var rb = GetComponent<Rigidbody2D>();
            if (collision.tag == "Knife" && collision.transform.parent != null && collision.transform.parent.tag == "Wood")
            {
                transform.SetParent(null);
                rb.gravityScale = 1;
                Debug.LogError("LOOSER");
            }

            if (collision.tag == "Wood")
            {
                rb.velocity = Vector2.zero;
                transform.SetParent(collision.transform);
                collision.GetComponent<Wood>().Hit(this);
            }

            if (OnHit != null)
            {
                OnHit.Invoke(this, collision);
            }
        }

        public void Throw(float force)
        {
            transform.SetParent(null);
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * force, ForceMode2D.Impulse);
        }
        void OnBecameInvisible()
        {
            gameObject.SetActive(false);
        }
    }
}