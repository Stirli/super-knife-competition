﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public interface IDamager
    {
        float Damage { get; }
    }
}