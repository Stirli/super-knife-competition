﻿using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts
{
    [System.Serializable]
    public class MyHitEvent : UnityEvent<MonoBehaviour, Collider2D>
    {

    }
}