﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : AbstractMenu
{


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            gameObject.SetActive(true);
        }
    }

    public override void Show()
    {
        base.Show();
        Debug.Log("PAUSE ON");
        Time.timeScale = 0f;
    }
    public override void Hide()
    {
        base.Hide();
        Debug.Log("PAUSE OFF");
        Time.timeScale = 1f;
    }

    public void GoToMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene("Level");
    }
}
