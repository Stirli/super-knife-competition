﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MenuHotKeys : MonoBehaviour
{
    // Start is called before the first frame update

    public List<AbstractMenu> menus = new List<AbstractMenu>();

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            var activeMenu = menus.LastOrDefault(m => m.isActiveAndEnabled);
            if (activeMenu!=null)
            {
                activeMenu.Hide();
            }
            else
            {
                menus.First().Show();
            }
        }
    }
}
