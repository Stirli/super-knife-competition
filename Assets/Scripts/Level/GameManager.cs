﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public UnityEvent OnWinEvent;
    public UnityEvent OnLooseEvent;

    

    public void OnKnifeHited(MonoBehaviour sender, Collider2D collision)
    {
        if (collision.tag == "Knife" && collision.transform.parent != null && collision.transform.parent.tag == "Wood")
        {
            if (OnLooseEvent != null)
            {
                OnLooseEvent.Invoke();
            }
        }
        if (collision.tag == "Wood")
        {
            if (collision.GetComponent<Wood>().hitpoints == 0)
            {
                if (OnWinEvent != null)
                {
                    OnWinEvent.Invoke();
                }
            }
        }
    }

    public enum GameState
    {

        Paused
    }
}
