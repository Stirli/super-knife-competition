﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveableBehaviour : MonoBehaviour
{
    void OnBecameInvisible()
    {
        Destroy(this);
    }
}
