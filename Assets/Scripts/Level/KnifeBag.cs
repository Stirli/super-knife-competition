﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class KnifeBag : MonoBehaviour
    {
        public int volume = 8;
        public Knife knifePrefab;

        private Queue<Knife> knives;

        public int Count => knives.Count;

        public GameManager gameManager;


        void Awake()
        {
            knives = new Queue<Knife>();
            var count = volume - knives.Count;
            for (int i = 0; i < count; i++)
            {
                var knife = Instantiate(knifePrefab, transform);
                knife.OnHit.AddListener(gameManager.OnKnifeHited);
                knife.transform.localPosition = new Vector3(i * knife.transform.localScale.x, 0);
                knives.Enqueue(knife);
            }
        }


        public Knife GetKnife(Transform parent)
        {
            var knife = knives.Dequeue();
            return knife;
        }
    }
}