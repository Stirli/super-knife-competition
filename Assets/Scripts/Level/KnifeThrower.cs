﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts
{
    public class KnifeThrower : MonoBehaviour
    {
        public float force = 25;
        public KnifeBag knifeBag;

        private Knife knife;

        // Start is called before the first frame update
        void Start()
        {
            knife = GetKnife();
        }

        // Update is called once per frame
        void Update()
        {

            if (Time.timeScale!=0f && Input.GetMouseButtonDown(0) && knife != null)
            {
                knife.GetComponent<Knife>().Throw(force);

                if (knifeBag.Count != 0)
                {
                    knife = GetKnife();
                }
                else
                {
                    knife = null;
                }

            }
        }

        Knife GetKnife()
        {
            var k = knifeBag.GetKnife(transform);

            k.transform.position = transform.position;
            k.transform.SetParent(transform);
            k.transform.localScale = transform.localScale;

            return k;
        }
        
    }
}