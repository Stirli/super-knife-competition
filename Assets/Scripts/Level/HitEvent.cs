﻿using UnityEngine.Events;

namespace Assets.Scripts
{
    [System.Serializable]
    public class HitEvent : UnityEvent<int>
    {

    }
}