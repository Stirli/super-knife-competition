﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class Wood : MonoBehaviour
    {
        public float rotationSpeed = 0;

        public int hitpoints = 10;

        private void FixedUpdate()
        {
            transform.Rotate(new Vector3(0, 0, rotationSpeed));
            if (rotationSpeed > 0.2f)
            {
                rotationSpeed -= 0.008f;
            }
            else
            {
                rotationSpeed = 0.2f;
            }

        }

        internal void Hit(IDamager damager)
        {
            if (rotationSpeed < 4)
                rotationSpeed += 1f;
            hitpoints -= (int)damager.Damage;
        }
    }
}